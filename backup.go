package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	// ToDo: Checks that binary can access docker api

	argLen := len(os.Args[1:])
	if argLen == 0 {
		arg := "help"
		help(arg)
	} else {
		arg := string(os.Args[1])
		switch arg {
		case "help":
			help(arg)
		case "backup":
			if argLen != 2 {
				arg := "backup"
				help(arg)
			} else {
				opt := os.Args[2]
				backup(opt)
			}
		}
	}
	os.Exit(1)
}

func help(cmd string) {
	switch cmd {
	case "help":
		fmt.Println("Options are [help, backup]")
		os.Exit(0)
	case "backup":
		fmt.Println("Backup option requires a volume name")
		os.Exit(0)
	}
}

func backup(volume string) {
	fmt.Println("Backing up volume[ ", volume, " ]")
	cmd := []string{"docker run --rm --volumes-from ", volume, " -v $(pwd):/backup ubuntu tar cvf /backup/" + volume + ".tar", " /data"}
	fmt.Println("Running command[", strings.Join(cmd, ""), "]")
}
